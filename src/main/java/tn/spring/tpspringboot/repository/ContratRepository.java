package tn.spring.tpspringboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.spring.tpspringboot.Entity.Contrat;
@Repository
public interface ContratRepository extends JpaRepository<Contrat, Integer> {
}
