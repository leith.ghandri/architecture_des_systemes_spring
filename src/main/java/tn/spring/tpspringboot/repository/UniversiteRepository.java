package tn.spring.tpspringboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.spring.tpspringboot.Entity.Universite;
@Repository
public interface UniversiteRepository extends JpaRepository<Universite,Integer> {
}
