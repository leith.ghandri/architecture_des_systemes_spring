package tn.spring.tpspringboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.spring.tpspringboot.Entity.Departement;
@Repository
public interface DepartementRepository extends JpaRepository<Departement, Integer> {
}
