package tn.spring.tpspringboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.spring.tpspringboot.Entity.Equipe;
@Repository
public interface EquipeRepository extends JpaRepository<Equipe,Integer> {
}
