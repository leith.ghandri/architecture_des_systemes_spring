package tn.spring.tpspringboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.spring.tpspringboot.Entity.DetailEquipe;
@Repository
public interface DetailEquipeRepository extends JpaRepository<DetailEquipe, Integer> {
}
