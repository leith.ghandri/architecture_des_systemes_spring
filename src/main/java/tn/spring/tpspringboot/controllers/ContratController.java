package tn.spring.tpspringboot.controllers;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.spring.tpspringboot.Entity.Contrat;
import tn.spring.tpspringboot.services.ContratService;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/contrat")
public class ContratController {
    @Autowired
    private ContratService contratService;

    @PostMapping("/save")
    public ResponseEntity<String> AddContrat(@RequestBody Contrat contrat){
        contratService.create(contrat);
        return new ResponseEntity<>("contrat registred succfully", HttpStatus.OK);
    }
    @GetMapping("/list")
    public Collection<Contrat> list(){
        return contratService.list();
        //return new ResponseEntity<>("contrat list", HttpStatus.OK);
    }
}
