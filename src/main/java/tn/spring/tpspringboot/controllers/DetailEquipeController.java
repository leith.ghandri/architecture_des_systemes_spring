package tn.spring.tpspringboot.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.spring.tpspringboot.Entity.DetailEquipe;
import tn.spring.tpspringboot.Entity.Etudiant;
import tn.spring.tpspringboot.services.DetailEquipeService;


@RestController
@RequestMapping("/detail")
public class DetailEquipeController {
    @Autowired
    private DetailEquipeService detailEquipeService;

    @PostMapping("/save")
    public ResponseEntity<String> AddEtudiant(DetailEquipe detailEquipe){
        detailEquipeService.create( detailEquipe);
        return new ResponseEntity<>(" detailEquipe registred succfully", HttpStatus.OK);
    }
}
