package tn.spring.tpspringboot.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.spring.tpspringboot.Entity.Etudiant;
import tn.spring.tpspringboot.Entity.Universite;
import tn.spring.tpspringboot.services.UniversiteService;


@RestController
@RequestMapping("/universite")
public class UniversiteController {
    @Autowired
    private UniversiteService universiteService;

    @PostMapping("/save")
    public ResponseEntity<String> AddUniversite(Universite universite){
        universiteService.create(universite);
        return new ResponseEntity<>("universite registred succfully", HttpStatus.OK);
    }
}
