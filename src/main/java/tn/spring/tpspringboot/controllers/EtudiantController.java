package tn.spring.tpspringboot.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.spring.tpspringboot.Entity.Contrat;
import tn.spring.tpspringboot.Entity.Etudiant;
import tn.spring.tpspringboot.services.EtudiantService;


@RestController
@RequestMapping("/etudiant")
public class EtudiantController {
    @Autowired
    private EtudiantService etudiantService;

    @PostMapping("/save")
    public ResponseEntity<String> AddEtudiant(Etudiant etudiant){
        etudiantService.create(etudiant);
        return new ResponseEntity<>("etudiant registred succfully", HttpStatus.OK);
    }
}
