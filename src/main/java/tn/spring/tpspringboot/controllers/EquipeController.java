package tn.spring.tpspringboot.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.spring.tpspringboot.Entity.Contrat;
import tn.spring.tpspringboot.Entity.Equipe;
import tn.spring.tpspringboot.services.EquipeService;


@RestController
@RequestMapping("/equipe")
public class EquipeController {
    @Autowired
    private EquipeService equipeService;

    @PostMapping("/save")
    public ResponseEntity<String> AddEquipe(Equipe equipe){
        equipeService.create(equipe);
        return new ResponseEntity<>("equipe registred succfully", HttpStatus.OK);
    }
}
