package tn.spring.tpspringboot.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.spring.tpspringboot.Entity.Departement;
import tn.spring.tpspringboot.Entity.Etudiant;
import tn.spring.tpspringboot.services.DepartementService;

@RestController
@RequestMapping("/departement")
public class DepartementController {
    @Autowired
    private DepartementService departementService;

    @PostMapping("/save")
    public ResponseEntity<String> AddDepartement(Departement departement){
        departementService.create(departement);
        return new ResponseEntity<>("departement registred succfully", HttpStatus.OK);
    }
}
