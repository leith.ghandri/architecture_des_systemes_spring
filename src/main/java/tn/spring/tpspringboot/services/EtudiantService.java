package tn.spring.tpspringboot.services;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.spring.tpspringboot.Entity.Contrat;
import tn.spring.tpspringboot.Entity.Etudiant;
import tn.spring.tpspringboot.repository.EtudiantRepository;

@Service
public class EtudiantService {
    @Autowired
    private EtudiantRepository etudiantRepository;

    public boolean create(Etudiant etudiant){
        etudiantRepository.save(etudiant);
        return true;
    }
}
