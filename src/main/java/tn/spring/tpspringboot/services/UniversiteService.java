package tn.spring.tpspringboot.services;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.spring.tpspringboot.Entity.Contrat;
import tn.spring.tpspringboot.Entity.Universite;
import tn.spring.tpspringboot.repository.UniversiteRepository;

@Service

public class UniversiteService {
    @Autowired
    private UniversiteRepository universiteRepository;

    public boolean create(Universite universite){
        universiteRepository.save(universite);
        return true;
    }
}
