package tn.spring.tpspringboot.services;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.spring.tpspringboot.Entity.Contrat;
import tn.spring.tpspringboot.Entity.DetailEquipe;
import tn.spring.tpspringboot.repository.DetailEquipeRepository;

@Service

public class DetailEquipeService {
    @Autowired
    private DetailEquipeRepository detailEquipeRepository;

    public boolean create(DetailEquipe detailEquipe){
        detailEquipeRepository.save(detailEquipe);
        return true;
    }
}
