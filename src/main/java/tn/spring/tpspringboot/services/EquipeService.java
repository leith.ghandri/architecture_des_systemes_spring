package tn.spring.tpspringboot.services;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.spring.tpspringboot.Entity.Contrat;
import tn.spring.tpspringboot.Entity.Equipe;
import tn.spring.tpspringboot.repository.EquipeRepository;

@Service
public class EquipeService {
    @Autowired
    private EquipeRepository equipeRepository;

    public boolean create(Equipe equipe){
        equipeRepository.save(equipe);
        return true;
    }
}
