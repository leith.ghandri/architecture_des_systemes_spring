package tn.spring.tpspringboot.services;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.spring.tpspringboot.Entity.Contrat;
import tn.spring.tpspringboot.repository.ContratRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ContratService {
    @Autowired
    private  ContratRepository contratRepository;

    public boolean create(Contrat contrat){
        contratRepository.save(contrat);
        return true;
    }

    public Collection<Contrat> list(){
        return contratRepository.findAll();
    }

    public Contrat get(int id){
        return contratRepository.findById(id).get();
    }

    public boolean delete(int id){
        if(contratRepository.findById(id).isPresent()==true){
            contratRepository.deleteById(id);
            return true;
        }else{
            return false;
        }
    }

    public void update(int id, Contrat contrat){
        Optional<Contrat> contrat1= contratRepository.findById(id);
        contrat1.get().setDateDebutContrat(contrat.getDateDebutContrat());
        contrat1.get().setDateFinContrat(contrat.getDateFinContrat());
        contrat1.get().setSpecialite(contrat.getSpecialite());
        contrat1.get().setArchive(contrat.getArchive());
        contratRepository.save(contrat1.get());
    }

}
