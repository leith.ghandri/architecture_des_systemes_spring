package tn.spring.tpspringboot.services;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.spring.tpspringboot.Entity.Contrat;
import tn.spring.tpspringboot.Entity.Departement;
import tn.spring.tpspringboot.repository.DepartementRepository;

@Service
public class DepartementService {
    @Autowired
    private DepartementRepository departementRepository;

    public boolean create(Departement departement){
        departementRepository.save(departement);
        return true;
    }
}
