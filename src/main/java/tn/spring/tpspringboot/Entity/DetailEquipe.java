package tn.spring.tpspringboot.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DetailEquipe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int salle;
    private String thematique;

    @OneToOne(mappedBy = "detailEquipe", cascade = CascadeType.PERSIST, orphanRemoval = true)
    private Equipe equipe;

}
