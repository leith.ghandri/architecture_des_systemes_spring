package tn.spring.tpspringboot.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Etudiant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private int idEtudiant;
    private String prenomE;
    private String nomE;
    @Enumerated
    private Option option;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "departement_id_depart")
    private Departement departement;

    @OneToMany(mappedBy = "etudiant", cascade = CascadeType.PERSIST, orphanRemoval = true)
    private List<Contrat> contrats;

    @ManyToMany
    @JoinTable(name = "etudiant_equipes",
            joinColumns = @JoinColumn(name = "etudiant_"),
            inverseJoinColumns = @JoinColumn(name = "equipes_id_equipe"))
    private List<Equipe> equipes;

}
