package tn.spring.tpspringboot.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Contrat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idContrat;
    private Date dateDebutContrat;
    private Date dateFinContrat;
    private Specialite specialite;
    private boolean archive;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "etudiant_id")
    private Etudiant etudiant;

    public boolean getArchive(){
        return this.archive;
    }
}
